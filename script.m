p=@(t) 0*t;
q=@(t) 4+0*t;
r=@(t) -4*t;
intv=[0 1];
a=0;
b=2;
c1=0;
c2=0;
N=10;

%%

% h y t
t0 = intv(1);
T = intv(2);
h = (T - t0)/(N + 1);
t1 = t0:h:T;
t2 = t1(2:end-1); 

% Matriz A(h)
m = length(t2);
l = [ -(1 + (h/2) * p(t2)); 2 + h^2 * q(t2); -(1 - (h/2) * p(t2))];
A1 = spdiags(l',-1:1,m,m);
A = full(A1);

% Vector B(h)
R = r(t2);
B1 = [a * (1 + (h/2)*p(t2(1))), zeros(1,m-2) , b *(1 - (h/2)*p(t2(end)))];
B = -h^2 * R + B1;

% sol x = A\B
x = A\B';

% sol u
u = [a; x; b]';