function[T,Y,j] = mirk4(f, intv, y0, N)

a = min(intv);
b = max(intv);
h = (b - a)/N;
 
f1 = f;
f2 = @(t,y) [ f((t + h/2), (y + (h/2) * f1(t,y))) ];
f3 = @(t,y) [ f((t + h/2), (y + (h/2) * f2(t,y))) ];
f4 = @(t,y) [ f((t + h), (y + h * f3(t,y))) ];
phi = @(t,y) (1/6)*(f1(t,y) + 2*f2(t,y) + 2*f3(t,y) + f4(t,y));

t = a;
y = y0;
j = 0;

T = t;
Y = y;

for k=1:N
    
    j = j + 4;

    l1 = phi(t,y);

    t = t + h;
    y = y + h * l1; 

    T = [T,t];
    Y = [Y,y];
end

end