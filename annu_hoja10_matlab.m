%% Prácticas de Matlab
%% Método de diferencia finitas
%% Hoja 10
% *Nombre: Hugo*
% 
% *Apellido: Del Castillo*
% 
% *DNI:*
% 
% *Email:*
%% 
% % 
% %% Práctica 1 (Implementación del método del disparo lineal)
% Crea una función  *midifflin.m* que, tomando como datos los coeficientes  
% $p(t)$,  $q(t)$, $r(t)$ de la ecuación diferencial, implemente el método del 
% disparo lineal para  las condiciones de contorno siguientes:
% 
% $$    \begin{array}{ccc}  x(t_{0})  &=&a, \qquad x(T)=b\end{array}$$
% 
% y que responda a la sintaxis
% 
% |[t,u]=midifflin(p,q,r,interval,a,b,N)|
% 
% *Indicación*: usa el comando *spdiags *para construir las matriz $A$.
%% Práctica 2 Resolución (D-D)
% Utiliza las implementaciones anteriores para resolver los siguientes problemas 
% de contorno. Explora las soluciones modificando las condiciones de contorno. 
% (N=10)
% 
% $$x''(t)=4(x(t)-t), \quad 0\leq t\leq 1 \quad    x(0)=-5, x(1)=2$$
% 
% *Solución*

clear all
close all
disp('H10: cod UB P1')
p=@(t) 0*t;
q=@(t) 4+0*t;
r=@(t) -4*t;
intv=[0 1];
a=0;
b=2;
c1=0;
c2=0;
N=10;
[t1,u1]=midisplin(p,q,r,intv,a,b,N,c1,c2);
[t2,u2]=midiffin(p,q,r,intv,a,b,N);
grid on
hold on
plot(t1,u1(1,:),'r-*'), 
plot(t1,u1(2,:),'b-*'), 
plot(t2,u2(1,:),'m-*'), 
legend('solucion-disp','derivada-disp','solucion-diff-fin')
grid on
s=sprintf(' \n (D-D) con N=%g',N);
title(['Disparo lineal; Diferencias-finitas : $x^{\prime\prime}(t)=4(x(t)-t) \quad 0\leq t\leq 1 \qquad     x(0)=-5 \quad x(1)=2$' s],'Interpreter','latex')
set(gca,'FontSize',14);
hold off
%% Apéndice
