function [t, u] = midisplin(p,q,r,intv,a,b,N,c1,c2)

y_nh = @(k,y) [y(2);  p(k)*y(2) + q(k)*y(1) + r(k)];
y_h = @(k,y) [y(2);  p(k)*y(2) + q(k)*y(1)];

% c1
if c1 == 0 % c1 = 0 => ( D-D | D-N ) 
    [t, v1] = mirk4(y_nh, intv, [a; 0], N); % no homo
    [~, v2] = mirk4(y_h, intv, [0; 1], N); % homo
else % c1 = 1 => ( N-D | N-N )
    [t, v1] = mirk4(y_nh, intv, [0;a], N); % no homo
    [~, v2] = mirk4(y_h, intv, [1;0], N); % homo
end

% c2
if c2 == 0 % c2 = 0 => ( D-D | N-D )
    if v2(1, N + 1) ~= 0 % sol uni.
        s = ((b - v1(1, N + 1)) / v2(1, N + 1));
        u = v1 + s * v2;
    else
        if v1(1, T + 1) == b % sol inf.
            u = v1;
        else % no sol
            t = [];
            u = [];
        end
    end
else % c2 = 1 => ( D-N | N-N ) 
    if v2(2, N + 1) ~= 0 % sol uni.
        s = ((b - v1(2, N + 1)) / v2(2, N + 1));
        u = v1 + s * v2;
    else
        if v1(2, T + 1) == b % sol inf.
            u = v1;
        else % no sol
            t = [];
            u = [];
        end
    end
end

end